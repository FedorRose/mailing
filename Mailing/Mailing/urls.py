from django.contrib import admin
from django.urls import path, include
from rest_framework.routers import DefaultRouter
from main.views import *

router = DefaultRouter()
router.register(r'client', ClientViewSet)
router.register(r'mailing', MailingViewSet)


urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/', include(router.urls)),
    path('', include('main.urls')),
]
