from django.contrib import admin
from .models import *

import csv
import datetime
from django.http import HttpResponse


def export_to_csv(modeladmin, request, queryset):
    opts = modeladmin.model._meta
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename={}.csv'.format(opts.verbose_name)
    writer = csv.writer(response)
    fields = ['Client', 'Mailing Tag', 'Status']
    fieldss = ['client', 'mailing', 'status']
    writer.writerow([field for field in fields])
    for obj in queryset:
        data_row = []
        for field_name in fieldss:
            value = getattr(obj, field_name)
            if isinstance(value, datetime.datetime):
                value = value.strftime('%d/%m/%Y')
            data_row.append(value)
        writer.writerow(data_row)
    return response
export_to_csv.short_description = 'Export to CSV'


class MessageAdmin(admin.ModelAdmin):
    model = Message
    list_display = ('time_create', 'status', 'mailing', 'client')
    actions = [export_to_csv]


class MailAdmin(admin.ModelAdmin):
    model = Mail
    list_display = ('tag', 'time_start', 'time_end', 'text', 'code',)
    list_display_links = ('tag', 'time_start', 'time_end')


class ClientAdmin(admin.ModelAdmin):
    model = Client
    list_display = ('number', 'tag', 'code_operator', 'time_zone',)


admin.site.register(Client, ClientAdmin)
admin.site.register(Mail, MailAdmin)
admin.site.register(Message, MessageAdmin)
