import os
from celery import Celery
from django.conf import settings
from celery.schedules import crontab


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'Mailing.settings')

app = Celery('main')

app.config_from_object('django.conf:settings')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

app.conf.beat_schedule = {
    'statistic': {
        'task': 'main.tasks.statistic',
        'schedule': crontab(minute=0, hour=0),
    }
}