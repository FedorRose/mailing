from .models import *
from django import forms
from django.forms import *


class EditMail(ModelForm):
    class Meta:
        model = Mail
        fields = ('time_start', 'time_end', 'text', 'code', 'tag',)


class EditClient(ModelForm):
    class Meta:
        model = Client
        fields = ('number', 'code_operator', 'tag', 'time_zone',)
