# Generated by Django 4.0.4 on 2022-04-14 10:45

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Client',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number', models.CharField(max_length=10)),
                ('code_operator', models.PositiveSmallIntegerField()),
                ('tag', models.PositiveSmallIntegerField()),
                ('time_zone', models.PositiveSmallIntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Mail',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('time_start', models.DateTimeField()),
                ('time_end', models.DateTimeField()),
                ('text', models.TextField()),
                ('code', models.PositiveSmallIntegerField()),
                ('tag', models.PositiveSmallIntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('time_create', models.DateTimeField(auto_now_add=True)),
                ('status', models.BooleanField(default=False)),
                ('client', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='main.client', verbose_name='client')),
                ('mailing', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='main.mail', verbose_name='mailing')),
            ],
        ),
    ]
