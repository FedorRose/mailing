# Generated by Django 3.2.13 on 2022-04-15 08:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0002_alter_message_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='status',
            field=models.BooleanField(default=False, verbose_name='status'),
        ),
        migrations.AlterField(
            model_name='message',
            name='time_create',
            field=models.DateTimeField(auto_now_add=True, verbose_name='time_create'),
        ),
    ]
