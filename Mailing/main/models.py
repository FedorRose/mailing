from django.db import models
from django.db.models import *


class Mail(Model):
    time_start = DateTimeField()
    time_end = DateTimeField()
    text = TextField()
    code = PositiveSmallIntegerField()
    tag = PositiveSmallIntegerField()

    def __str__(self):
        return str(self.tag)


class Client(Model):
    number = CharField(max_length=10)
    code_operator = PositiveSmallIntegerField()
    tag = PositiveSmallIntegerField()
    time_zone = PositiveSmallIntegerField()

    def __str__(self):
        return str(self.number)


class Message(Model):
    time_create = DateTimeField(auto_now_add=True, verbose_name='time_create')
    status = BooleanField(default=True, verbose_name='status')
    mailing = ForeignKey(Mail, verbose_name='mailing', on_delete=PROTECT)
    client = ForeignKey(Client, verbose_name='client', on_delete=PROTECT)
