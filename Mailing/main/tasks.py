import pytz
from django.core.mail import send_mail, EmailMessage
import datetime
import requests
from .celery import app
from .models import *
from . import views

'''
Задачи для Celery
Первый метод реализует рассылку. Если текущее время больше времени начала и меньше времени окончания, 
то из справочника выбираются все контакты, подходящие под фильтри запускается отправка.
Если время старта еще не наступило, то задача откладывается и выполняется в нужное время без дополнительных действий.
Второй метод реализует рассылку отчета за день каждые сутки 
'''


@app.task
def send_mail(ids):
    time = datetime.datetime.today()
    tz = pytz.timezone("Europe/Moscow")
    time = tz.localize(time)
    mail = Mail.objects.get(pk=ids)
    time_start = mail.time_start
    time_end = mail.time_end
    time_start = time_start.replace(tzinfo=pytz.utc).astimezone(tz)
    time_end = time_end.replace(tzinfo=pytz.utc).astimezone(tz)

    if time_start < time < time_end:
        clients = Client.objects.filter(tag=mail.tag)
        for el in clients:
            m = Message(mailing=mail, client=el)
            m.save(force_insert=True)

            data = {"id": m.pk, "phone": m.client.number, "text": m.mailing.text}
            head = {
                'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2ODEzODA2OTAsImlzcyI6ImZhYnJpcXVlIiwibmFtZSI6IlJvc2UifQ.IjBHm4opNf_0G0o_2Ndy23it2CdaE2admSVzv7hAtR0',
                'Accept': 'application/json', 'Content-Type': 'application/json'}
            req = requests.post('https://probe.fbrq.cloud/v1/send/{}'.format(m.pk), headers=head, json=data)
            views.write_message(pk=m.pk)

    elif time_start > time:
        send_mail.apply_async(args=(ids,), eta=time_start)


@app.task
def statistic(*args, **kwargs):
    time = datetime.datetime.today()
    email = EmailMessage(
        'Отчет',
        'Статистика за {} '.format(time),
        'fkolonistov1@ya.ru',
        ['fkolonistov@ya.ru'])
    views.write_message()
    email.attach_file('otchetDay.csv')
    email.send()
