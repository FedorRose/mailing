from django.urls import path, include
from main.views import *


urlpatterns = [
    path('', views.admin_view, name='admin'),
    path('<slug:key_slug>/<int:pk>/', views.admin_edit, name='edit'),
]
