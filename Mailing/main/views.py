import datetime
from django.shortcuts import render, redirect
from rest_framework import mixins, status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from .forms import *
from .serializers import *
from .models import *
from .tasks import *
import csv
from django.http import HttpResponse

'''
Классы ViewSet реализуют CRUD, а также просмотр списка и отдельной записи для сущностей Клиент и Рассылка.
Метод write_message собирает статисктику в csv файл по каждому отправленному сообщению. А также формирует отчет за сутки.
Методы admin_view и admin_edit - администраторский web интерфейс для работы со справочником пользователей и расслыками.
'''


class ClientViewSet(mixins.CreateModelMixin,
                    mixins.RetrieveModelMixin,
                    mixins.UpdateModelMixin,
                    mixins.ListModelMixin,
                    mixins.DestroyModelMixin,
                    GenericViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer


class MailingViewSet(mixins.CreateModelMixin,
                     mixins.RetrieveModelMixin,
                     mixins.UpdateModelMixin,
                     mixins.ListModelMixin,
                     mixins.DestroyModelMixin,
                     GenericViewSet):
    queryset = Mail.objects.all()
    serializer_class = MailingSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)

        send_mail.delay(serializer.data['id'])

        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            instance._prefetched_objects_cache = {}

        send_mail.delay(serializer.data['id'])

        return Response(serializer.data)


def write_message(pk=None):
    if pk:
        with open("otchet.csv", mode="a", encoding='utf-8') as w_file:
            message = Message.objects.get(pk=pk)
            l = [message.client.number, message.mailing, message.status]
            writer = csv.writer(w_file)
            writer.writerow(l)

    if not pk:
        message = Message.objects.all()
        today = datetime.datetime.today()
        tz = pytz.timezone("Europe/Moscow")
        today = tz.localize(today)

        with open("otchetDay.csv", mode="w+", encoding='utf-8') as w_file:
            # w_file.truncate()
            writer = csv.writer(w_file)
            writer.writerow([today.date()])
            writer.writerow(['Client', 'Mailing_Tag', 'Status'])

            for el in message:
                el.time_create.replace(tzinfo=pytz.utc).astimezone(tz)
                if el.time_create.date() == today.date():
                    l = [el.client.number, el.mailing, el.status]
                    writer.writerow(l)


def admin_view(request):
    mailings = Mail.objects.all()
    clients = Client.objects.all()

    return render(request, 'main/index.html', {'mailings': mailings, 'clients': clients})


def admin_edit(request, key_slug, pk=None):
    if key_slug == 'edit_mail':
        mailingg = Mail.objects.get(pk=pk)
        form = EditMail(instance=mailingg)
        if request.method == 'POST':
            form = EditMail(request.POST)
            if form.is_valid():
                mailingg.time_start = request.POST.get("time_start")
                mailingg.time_end = request.POST.get("time_end")
                mailingg.text = request.POST.get("text")
                mailingg.code = request.POST.get("code")
                mailingg.tag = request.POST.get("tag")
                mailingg.save()
                return redirect('edit', key_slug, pk)

        return render(request, 'main/edit.html', {'form': form})

    if key_slug == 'edit_client':
        clientt = Client.objects.get(pk=pk)
        form = EditClient(instance=clientt)
        if request.method == 'POST':
            form = EditClient(request.POST)
            if form.is_valid():
                clientt.number = request.POST.get("number")
                clientt.code_operator = request.POST.get("code_operator")
                clientt.tag = request.POST.get("tag")
                clientt.save()
                return redirect('edit', key_slug, pk)

        return render(request, 'main/edit.html', {'form': form})

